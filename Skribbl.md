# Mathe-Skribbl
1. [Grundlagen](#grundlagen)
   - [Gruppen, Ringe und Körper](#gruppen-ringe-körper)
   - [Geometrie](#geometrie)
   - [Permutationen](#permutationen)
2. [Analysis](#analysis)
3. [Lineare Algebra](#lineare-algebra)


# Grundlagen:

Aussagenlogik, Aussage, Wahrheitswert, wahre Aussage, falsche Aussage, Tautologie, Widerspruch,
Junktor, genau dann wenn, äquivalent, folgt aus, und, oder, entweder oder, nicht, Negation, De Morgansche Regeln, Idempotenz,
Prädikatenlogik, Quantor, Existenzquantor, Allquantor,

Beweis, direkter Beweis, indirekter Beweis, Beweis durch Widerspruch, Kontraposition, Fallunterscheidung, Umkehrung, Beweisschritt,
Induktion, vollständige Induktion, induktiv, Induktionsschritt, Induktionsanfang, Induktionsvorraussetzung,
Axiom, axiomatisch, Folgerung, Lemma, Satz, Korollar, Implikation, Behauptung, Notation, Definition, Konvention, Struktur, Ansatz, Bedingung,
eindeutig, hinreichend, notwendig, wohldefiniert, repräsentantenunabhängig, trivial, nichttrivial, intrinsisch, redundant, sukzessive,
Kriterium, hinreichendes Kriterium, Exkurs, allgemein, spezifisch, ausklammern,
punktweise, komponentenweise, repräsentantenweise, paarweise, analog, Existenz, Konstruktion, konstruieren, induzieren, kanonisch,
alternativ, alternativer Beweis, implizit, explizit, rekursiv, kompatibel, orientiert, formal, präzise, heuristisch, Heuristik, abstrakt, konkret,
Parametrisierung, Maßstab, Eigenschaft, ändern, Änderung, Index, Indices, Variable, Konstante, variabel, konstant,
gleich, verschieden, paarweise verschieden,

Mengenlehre, Menge, Familie, Tupel, Element, leere Menge, Teilmenge, echte Teilmenge, unechte Teilmenge,
Durchschnitt, Schnittmenge, Vereinigung, disjunkte Vereinigung, disjunkt,
Differenzmenge, Komplement, Produktmenge, Potenzmenge, externe disjunkte Vereinigung,

Abbildung, Funktion, Zuordnung, Bild, Urbild, Faser, Kern, Einschränkung, Definitionsbereich, Zielbereich,
surjektiv, injektiv, bijektiv, Bijektion, Schubfachprinzip,
Verkettung, Komposition, Identitätsfunktion, Umkehrfunktion, Auswahlaxiom,

Relation, binäre Relation, Äquivalenzrelation, kongruent, Kongruenz, ähnlich, teilbar, modulo, Restklasse, Parität, Ziffer,
Äquivalenzklasse, Repräsentant, Repräsentantensystem, Quotient,
Ordnungsrelation, partielle Ordnung, totale Ordnung, vergleichbar, Vergleichbarkeit,
größtes Element, maximales Element, Wohlordnung,

kommutativ, assoziativ, distributiv, Kommutativität, Assoziativität, Distributivität, Kommutativgesetz, Assoziativgesetz, Distributivgesetz,
reflexiv, symmetrisch, transitiv, additiv, Reflexivität, Symmetrie, Antisymmetrie, Transitivität, Additivität, Multiplikativität,
alternierend,

natürlich, natürliche Zahl, natürliche Zahlen, Primzahl, ganze Zahl, ganze Zahlen,
Bruch, rational, rationale Zahlen, irrational, irrationale Zahlen, reell, reelle Zahlen,
komplex, imaginär, komplexe Zahlen, Realteil, Imaginärteil, positiv, negativ, Vorzeichen,

Operation, Addition, Subtraktion, Multiplikation, Linksmultiplikation, Rechtsmultiplikation, Division, Wurzel, Potenz, Exponent, Basis, Fakultät, Binomialkoeffizient,

## Gruppen, Ringe & Körper:

Gruppentheorie, Verknüpfung, Halbgruppe, abelsch, linksneutral, rechtsneutral, neutrales Element, Monoid,
Gruppe, linksinvers, rechtsinvers, invers, Inverses, abelsche Gruppe, kommutative Gruppe, abelsche Halbgruppe,
Untergruppe, triviale Untergruppe, Ordnung von Gruppen, Satz von Lagrange, zyklische Untergruppe, zyklisch,

Homomorphismus, Monomorphismus, Epimorphismus, Isomorphismus, Endomorphismus, Automorphismus,
isomorph, Gruppenhomomorphismus, Ringhomomorphismus,
Diedergruppe, kommutieren, kommutatives Diagramm, Diagramm, Quotient, Quotientenabbildung, Quotientengruppe, Homomorphiesatz für Gruppen,
Konjugation, normale Untergruppe, Normalteiler,

Ring, Null, Eins, Nullelement, Einselement, Nullring, Einheitengruppe, Einheit, Körper, Körperaxiome, endlicher Körper,
Teilring, Teilkörper, Charakteristik, Integritätsring, kürzen, Bézout-Identität, Division mit Rest, Euklidischer Algorithmus,
Euklidischer Ring, Gradfunktion, Polynom, Koeffizient, Grad, Monom, Polynomring, Polynomfunktion, Nullpolynom, Nullfunktion, Polynomdivision,

Ideal, Quotientenring, Hauptideal, Homomorphiesatz für Ringe, Hauptidealring,
assoziiert, Teiler, Primelement, prim, irreduzibles Element, irreduzibel, irreduzibles Polynom, normiertes Polynom, Primfaktorzerlegung,

## Geometrie

Gerade, Ebene, Ursprung, Kreis, Quadrat, Einheitskreis, Einheitsquadrat, Parallelogramm, parallel, Rechteck,
Länge, Seitenlänge, Fläche, Flächeninhalt, orientierte Fläche,
Drehung, Spiegelung, Achse, Scherung, Winkel, Cavalieri-Prinzip,

Raum, Volumen, orientiertes Volumen, Parallelotop, Einheitswürfel,

## Permutationen

Permutation, symmetrische Gruppe, Vertauschung, Transposition, Fehlstand, Signum, gerade Permutation, ungerade Permutation, alternierende Gruppe,
Zykel, disjunkte Zykel,


# Analysis:

angeordneter Körper, Intervall, abgeschlossenes Intervall, offenes Intervall, halboffenes Intervall,
Betrag, Positivität, Dreiecksungleichung, Vollständigkeitsaxiom, Dedekindscher Schnitt, Schnittzahl,

beschränkt, von oben beschränkt, von unten beschränkt, obere Schranke, untere Schranke,
Supremum, Infimum, Maximum, Minimum, Archimedisches Axiom, Intervallschachtelung,

abzählbar, überabzählbar, endlich, unendlich, Mächtigkeit, Kontinuumshypothese,

Binomischer Satz, Bernoullische Ungleichung, Geometrische Summe, komplexes Konjugat,

Skalarprodukt, Cauchy–Schwarz-Ungleichung,
metrischer Raum, Metrik, Symmetrie, Standardmetrik, Mannheimer-Metrik, französische Eisenbahnmetrik, diskrete Metrik,
induzierte Metrik, Produktmetrik, kartesisches Produkt, isometrisch, Isometrie,

Kugel, Inneres, innerer Punkt, Abschluss, Rand, Randpunkt, offen, abgeschlossen, Häufungspunkt,
isolierter Punkt, dicht, Umgebung,

Folge, konvergieren, konvergent, Grenzwert, divergieren, divergent, uneigentlich konvergent, bestimmt divergent,
Teilfolge, Nullfolge, Satz von Bolzano-Weierstraß, monoton wachsend, monoton fallend, monoton, Monotonie, Monotonieverhalten,
Limes, Limes Superior, Limes Inferior, Eulerzahl, Cauchy-Folge, vollständig, Vervollständigung,

folgenkompakt, total beschränkt, offene Überdeckung, Teilüberdeckung, endliche Teilüberdeckung, kompakt,
Satz von Heine-Borel, Topologie, topologischer Raum, zusammenhängend, Zusammenhangskomponente,

Norm, euklidische Metrik, euklidische Norm, Supremum-Norm, äquivalente Normen,
normiert, Normierung, normierter Vektorraum, Banachraum, Hilbertraum,

Reihe, Reihenglieder, Partialsumme, Partialsummenfolge, absolut-konvergent, geometrische Reihe, harmonische Reihe,
Cauchy-Kriterium, Majorantenkriterium, Wurzelkriterium, Quotientenkriterium, Abel-Dirichlet-Kriterium,
alternierende Reihe, Leibnitz-Kriterium, Leibnitz-Reihe, Fehlerabschätzung, Cauchy-Verdichtungskriterium,

Cauchy-Produkt, Umordnung, Umordnungssatz, Riemannscher Umordnungssatz, Potenzreihe, Konvergenzradius,
Exponentialfunktion, exp, Logarithmusfunktion, natürlicher Logarithmus, ln, log, Potenzgesetze,

linksseitiger Grenzwert, rechtsseitiger Grenzwert, stetig, folgenstetig, unstetig,
Unstetigkeitsstelle, hebbare Unstetigkeitsstelle, Sprungstelle, Sprung, Unstetigkeit zweiter Art,
rationale Funktion, gleichmäßig stetig, lipschitzstetig, Lipschitz-Konstante,

Zwischenwertsatz, bogenzusammenhängend, wegzusammenhängend, Wegzusammenhangskomponente, Weg,
Strecke, Verbindungsstrecke, konvex, sternförmig, topologischer Kamm, Satz von Weierstraß, Satz von Heine,
Homöomorphismus, homöomorph, stetige Umkehrfunktion, kontrahierend, Kontraktion, Fixpunkt, Banachscher Fixpunktsatz, Fixpunktiteration,

punktweise konvergent, Grenzfunktion, gleichmäßig konvergent, gleichmäßige Konvergenz, Grenzwert-Vertauschbarkeit,
Funktionenreihe, Weierstraß-Majorantenkriterium, Potenzreihen-Identitätssatz,

trigonometrische Funktionen, Sinus, Cosinus, sin, cos, gerade Funktion, ungerade Funktion,
Eulersche Formel, Additionstheoreme, Differenzformeln, Nullstelle, Pi, periodisch, Periode,
Tangens, Cotangens, tan, cot, Arcussinus, arcsin, Zweig, Arcuscosinus, arccos, Arcustangens, arctan,
Arcuscotangens, arccot,

Hyperbelfunktionen, Sinus hyperbolicus, sinh, Cosinus hyperbolicus, cosh, Tangens hyperbolicus, tanh,
Cotangenshyperbolicus, coth, Hyperbel, Seilkurve, Areasinus hyperbolicus, arsinh,
Areacosinus hyperbolicus, arcosh, Areatangens hyperbolicus, artanh, Areacotangens hyperbolicus, arcoth,

Fundamentalsatz der Algebra, Polynom-Zerlegungssatz, algebraische Vielfachheit,

Approximationssätze, approximieren, Weierstraß-Approximationssatz, Bernsteinpolynom,
Satz von Stone-Weierstraß, trigonometrisches Polynom,

Differentialrechnung, lokales Änderungsverhalten, differenzierbar, Ableitung, Sekante, Tangente, in erster Näherung, Differenzenquotient,
Produktregel, Quotientenregel, Kettenregel, erste Ableitung, zweite Ableitung, dritte Ableitung, stetig differenzierbar, glatt,

lokales Minimum, lokales Maximum, Satz von Rolle,
Mittelwertsatz von Cauchy, Mittelwertsatz von Lagrange, MWS, Differentialgleichung, DGL,
isoliertes lokales Minimum, isoliertes lokales Maximum,


konkav, konvex, konvexe Funktion, konkave Funktion, Sekantensteigung, lokal lipschitzstetig, Wendepunkt, Mittelwertsatz für Vektoren,
erste Regel von L'Hospital, zweite Regel von L'Hospital, differenzierbare Grenzfunktion,

## Analysis, coming soon

Zentrum, Konvergenzintervall, Abelscher Grenzwertsatz, Leibnizformel, Machinsche Formel,
Landau-Symbole, reell-analytisch, entwickelbar, Taylorentwicklung, Taylorreihe, Satz von Borel, Taylorpolynom,
Schmiegparabel, Restglied, Lagrange-Form des Restgliedes, Cauchy-Form des Restgliedes, Binomialreihe,
lokaler Extremwert,



# Lineare Algebra:

Lineare Algebra, Vektorraum, Vektor, Skalar, Linearkombination, Nullvektor, Nullraum, Standard-Vektorraum,
Zeilenvektor, Spaltenvektor, Skalarmultiplikation,
Untervektorraum, linearer Unterraum, Teilraum, Linearkombination, Aufspann, lineare Hülle,

Erzeugendensystem, endlich erzeugt, linear abhängig, linear unabhängig,
Basis, Standard-Basis, Basisvektoren, Standard-Basisvektoren, Kette, Zorns Lemma, Basisergänzungssatz, Basisaustauschsatz,

lineares Gleichungssystem, LGS, homogenes LGS, inhomogenes LGS, homogen, inhomogen, Lösungsmenge, lösbar, nicht lösbar, Lösung,

Dimension, endlichdimensional, Dimensionsformel, externe direkte Summe, interne Summe, interne direkte Summe, direkt,

lineare Abbildung, Vektorraumhomomorphismus, linear, Linearität, affin-linear, affin-lineare Abbildung, Projektion, Projektionsabbildung, Inklusion,
Linearitätskriterium, Struktursatz, Homomorphismenraum, Endomorphismenring, Algebra, Unteralgebra, Endomorphismenalgebra, Matrixalgebra,

Kovariante, Kontravariante, Linearform, Dualraum, Dualform, duale Abbildung,
Matrix, Zeile, Spalte, Zeilenindex, Spaltenindex, Abbildungsmatrix, Matrixprodukt,
Nullmatrix, Einheitsmatrix, Drehmatrix, Dreiecksmatrix, obere Dreiecksmatrix, untere Dreiecksmatrix,
Blockmatrix, Elementarmatrix, Kronecker-Delta,
duale Basis, Bidualität, Transponierte Matrix, transponieren,

affiner Unterraum, Spaltenrang, Rang, inverse Matrix, invertierbar, invertierbare Matrix, allgemeine lineare Gruppe,
Quotientenraum, Homomorphiesatz, Isomorphiesätze,

Sequenz, exakt, exakte Sequenz, kurze exakte Sequenz, Cokern, duale Sequenz,

Basiswechsel, Basiswechselmatrix, Transformationsformel, Blockmatrix, äquivalente Matrizen, ähnliche Matrizen,
Gauß-Algorithmus, Zeilenstufenform, reduzierte Zeilenstufenform, elementare Umformungen, elementare Transformationen, Zeilenumformungen, Spaltenumformungen,
elementare Umformung vom Typ 1, elementare Umformung vom Typ 2, elementare Umformung vom Typ 3, elementare Umformung vom Typ 4,
Koeffizientenmatrix, Kodimension,

Satz von Skolem-Noether, innerer Automorphismus,

Determinante, multilinear, Multilinearität, multilineare Abbildung, Multilinearform,
alternierende Abbildung, alternierende Multilinearform, Determinantenfunktion,

Blockdreiecksmatrix, Leibniz-Formel, Polynominterpolation, Interpolationsproblem, Vandermonde-Matrix, Vandermonde-Determinante,
Laplacescher Entwicklungssatz, komplementäre Matrix, Cramersche Formel, spezielle lineare Gruppe,

Diagonalmatrix, diagonalisierbar, Diagonaleintrag, Eigenwert, Eigenvektor, Eigenraum,
charakteristisches Polynom, Spur, invariant, Konjugationsinvarianz,
Linearfaktor, Nullstellenordnung, reelle Nullstelle, komplexe Nullstelle,
algebraisch abgeschlossen, algebraischer Abschluss, algebraische Zahlen, transzendent,
geometrische Vielfachheit,
